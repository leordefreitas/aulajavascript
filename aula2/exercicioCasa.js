let notas = [
  {
    "nome": "Maria",
    "turma": "A",
    "nota1": 10,
    "nota2": 5
  },
  {
    "nome": "Pedro",
    "turma": "A",
    "nota1": 10,
    "nota2": 8
  },
  {
    "nome": "Cristina",
    "turma": "B",
    "nota1": 5,
    "nota2": 2
  },
  {
    "nome": "Jucelina",
    "turma": "B",
    "nota1": 10,
    "nota2": 9
  },
  {
    "nome": "Jonathan",
    "turma": "C",
    "nota1": 9,
    "nota2": 9
  }
];

const acharMedia = (array) => {
  let maiorMediaA = {}; 
  let mediaA = 0;
  let mediaAnteriorA = 0;
  let maiorMediaB = {};
  let mediaB = 0;
  let mediaAnteriorB = 0;
  let maiorMediaC = {};
  let mediaC = 0;
  let mediaAnteriorC = 0;


  for(i in array) {
    if(array[i].turma === "A") {
      mediaA = (array[i].nota1 + array[i].nota2) / 2;
      if(mediaA > mediaAnteriorA) {
        mediaAnteriorA = mediaA;
        maiorMediaA = array[i];
      }
    }
    else if(array[i].turma === "B") {
      mediaB = (array[i].nota1 + array[i].nota2) / 2;
      if(mediaB > mediaAnteriorB) {
        mediaAnteriorB = mediaB;
        maiorMediaB = array[i];
      }
    }
    else if(array[i].turma === "C") {
      mediaC = (array[i].nota1 + array[i].nota2) / 2;
      if(mediaC > mediaAnteriorC) {
        mediaAnteriorC = mediaC;
        maiorMediaC = array[i];
      }
    }
  }
  return `A maior media da turma A foi do aluno(a) ${maiorMediaA.nome} com a media de ${mediaAnteriorA}\nA maior media da turma B foi do aluno(a) ${maiorMediaB.nome} com a media de ${mediaAnteriorB}\nA maior media da turma C foi do aluno(a) ${maiorMediaC.nome} com a media de ${mediaAnteriorC}`;
};

console.log(acharMedia(notas));