let frase = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit condimentum justo eget volutpat. Morbi felis diam, pellentesque sed aliquam eu, aliquam vitae elit. Integer ut sapien est. Nulla at elit nec nisl dignissim viverra sit amet in libero. Nunc sed bibendum enim. Praesent pharetra semper nulla vitae interdum. Nunc interdum enim imperdiet porta bibendum. Aenean ex ipsum, placerat sed sollicitudin et, tempus non sapien. Suspendisse congue vulputate molestie. Cras auctor vehicula justo tincidunt accumsan. Pellentesque vel iaculis nibh. Aliquam scelerisque eleifend vestibulum. Phasellus non dictum eros. Praesent cursus laoreet ipsum, in porta nisi hendrerit eu. Pellentesque scelerisque felis ut nunc sagittis, quis ultricies nunc euismod. Curabitur quis neque in magna efficitur luctus mollis vel odio. In eu condimentum orci. Curabitur ut ex imperdiet, consectetur diam at, vestibulum risus. Nunc pharetra, est eu placerat dapibus, risus odio blandit ex, at aliquam enim augue sit amet lacus. Cras bibendum, quam non ultrices porttitor, leo urna egestas eros, a sagittis ligula erat vitae purus. In sit amet porta turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae Cras mauris mi, aliquet ac dui non, pellentesque venenatis metus. Integer hendrerit tortor id pharetra ultrices. Suspendisse cursus suscipit congue. Vestibulum ornare faucibus interdum. Aliquam dapibus elit sed lorem laoreet tincidunt. Duis et sem fermentum urna tincidunt rutrum sit amet volutpat elit.";
let novaFrase = "";

for (let i = 0; i < frase.length; i++) {
  if (frase[i] === "a" || frase[i] === "A") novaFrase += "1";
  if (frase[i] === "b" || frase[i] === "B") novaFrase += "2";
  if (frase[i] === "c" || frase[i] === "C") novaFrase += "3";
  if (frase[i] === "d" || frase[i] === "D") novaFrase += "4";
  if (frase[i] === "f" || frase[i] === "F") novaFrase += "5";
  if (frase[i] === "g" || frase[i] === "G") novaFrase += "6";
  if (frase[i] === "h" || frase[i] === "H") novaFrase += "7";
  if (frase[i] === "i" || frase[i] === "I") novaFrase += "8";
  if (frase[i] === "j" || frase[i] === "J") novaFrase += "9";
  if (frase[i] === "k" || frase[i] === "K") novaFrase += "10";
  if (frase[i] === "l" || frase[i] === "L") novaFrase += "11";
  if (frase[i] === "m" || frase[i] === "M") novaFrase += "12";
  if (frase[i] === "n" || frase[i] === "N") novaFrase += "13";
  if (frase[i] === "o" || frase[i] === "O") novaFrase += "14";
  if (frase[i] === "p" || frase[i] === "P") novaFrase += "15";
  if (frase[i] === "q" || frase[i] === "Q") novaFrase += "16";
  if (frase[i] === "r" || frase[i] === "R") novaFrase += "17";
  if (frase[i] === "s" || frase[i] === "S") novaFrase += "18";
  if (frase[i] === "t" || frase[i] === "T") novaFrase += "19";
  if (frase[i] === "u" || frase[i] === "U") novaFrase += "20";
  if (frase[i] === "v" || frase[i] === "V") novaFrase += "21";
  if (frase[i] === "x" || frase[i] === "X") novaFrase += "22";
  if (frase[i] === "y" || frase[i] === "Y") novaFrase += "23";
  if (frase[i] === "z" || frase[i] === "Z") novaFrase += "24";
  if (frase[i] === ".") novaFrase += "0";
  if (frase[i] === ",") novaFrase += "-1";
  if (frase[i] === " ") novaFrase += " ";
};

console.log(novaFrase);