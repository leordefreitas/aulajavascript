// praticando alguns codigos
let primeiroNome = 'Leonardo';
let ultimoNome = 'Freitas';
let idade = 20;
let gostos = ["games", "futebol", "ler", "star wars"];

console.log('Meu nome e', primeiroNome, ultimoNome, 'e tenho', idade, 'anos.');

let tipoDoNome = typeof(primeiroNome);
let tipoDaIdade = typeof(idade);

console.log("O tipo do nome e da idade,", tipoDoNome, tipoDaIdade);
console.log("O nome e igual o sobrenome?", primeiroNome === ultimoNome);
console.log("Minha idade e maior que 25?", idade > 25);

if (gostos.length < 5) {
  console.log("Eu tenho menos que 5 gostos!");
} else console.log("Eu tenho mais que 5 gostos!");
console.log(`Meus gostos sao ${gostos}`);

let ano = 2020;
for (i = idade; i < 31; i++) {
  console.log(`Farei ${i} no de ${ano}`);
  ano += 1;
};

let f = 1;
let infinito = 85;
while (f <= 10) {
  console.log(`${infinito}`);
  f += 1;
  infinito *= 85;
};

let comprimentoPiramide = 10;
let letraPiramide = "";

while (comprimentoPiramide >= 1) {
  letraPiramide += "X";
  console.log(letraPiramide);
  comprimentoPiramide -= 1;
};

let nome = "Leonardo Rodrigues de Freitas";

for (l in nome) {
  console.log(nome[l]);
};