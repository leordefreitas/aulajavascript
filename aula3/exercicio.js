// exercicio 1
/* let arrayQualquer = [[1, 2, 3], [1, 2, 2], [1, 1]];

const func1 = (...arr) => {
  let soma = 0;
  arr.map((e) => {
    for(let i = 0; i < e.length; i++) {
      soma += e[i];
    };
  });
  return console.log(soma);
};

func1(arrayQualquer); */

// exercicio 2
/* let arrayQualquer = [1, 3, 6, 10];

const func2 = (num, ...arr) => {
  let novoArray = arr.map((e) => e * num);
  return console.log(novoArray);
};

func2(2, arrayQualquer) */

// exercicio 3
/* let arrayQualquer = ["abc", "ba", "ab", "bb", "kb"];

const func3 = (letra, variasLetras) => {
  let letraArray = letra.split("");
  let variasLetrasArray = [];
  for(let i = 0; i < variasLetras.length; i++) {
    variasLetrasArray.push(variasLetras[i].split(""));
  };
  let novoArray = [];
  for(let j = 0; j < variasLetrasArray.length; j++) {
    novoArray = letraArray.filter(e => variasLetrasArray[j].includes(e));
  };
  console.log(novoArray);
};

func3("ab", arrayQualquer); */

// exercicio 4
/* let arrayQualquer = [[1, 2, 3], [3, 3, 7], [9, 111, 3]];

const func4 = (arr) => {
  let primeiroArray = [...arr[0]];
  let novoArray = [];
  for(let i = 1; i < arr.length; i++) {
    novoArray = primeiroArray.filter(e => arr[i].includes(e));
  };
  console.log(novoArray);
}

func4(arrayQualquer); */


// exercicio 5
/* let arrayQualquer = [[1, 1, 3], [1, 2, 2, 2, 3], [2]];

const func5 = (arr) => {
  let novoArray = [];
  let soma = 0;
  for(let i = 0; i < arr.length; i++) {
    for(let j = 0; j < arr[i].length; j++) {
      soma += arr[i][j];
    }
    if(soma % 2 === 0) novoArray.push(arr[i]);
    soma = 0;
  }
  console.log(novoArray);
};

func5(arrayQualquer); */
